`timescale 1ns / 1ps

module mux2(
    input a,
    input b,
    input sel,
    output out
    );

	 assign out = (sel) ? b : a;

endmodule

module mux16(
    input [15:0] a,
    input [15:0] b,
    input sel,
    output [15:0] out
    );
	 
	 assign out = (sel) ? b : a;
	 
endmodule
