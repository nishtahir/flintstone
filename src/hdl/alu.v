`timescale 1ns / 1ps

module alu (
        input [15:0] x,
        input [15:0] y,
        input zx,
        input nx,
        input zy,
        input ny,
        input f,
        input no,
        output [15:0] out,
        output zr,
        output ng
    );
    
    wire[15:0] zx_wire;
    wire[15:0] nx_wire;
    
    wire[15:0] zy_wire;
    wire[15:0] ny_wire;

    wire[15:0] and_xy_wire;
    wire[15:0] add_xy_wire;

    wire[15:0] out_wire;

    mux16 zx_mux (
        .a(x),
        .b(0),
        .sel(zx),
        .out(zx_wire)
    );
    
    mux16 nx_mux (
        .a(zx_wire),
        .b(~zx_wire),
        .sel(nx),
        .out(nx_wire)
    );
    
    mux16 zy_mux (
        .a(y),
        .b(0),
        .sel(zy),
        .out(zy_wire)
    );
    
    mux16 ny_mux (
        .a(zy_wire),
        .b(~zy_wire),
        .sel(ny),
        .out(ny_wire)
    );

    and16 xy_and(
        .a(nx_wire),
        .b(ny_wire),
        .out(and_xy_wire)
    );

    ripple_carry_adder16 xy_add (
        .a(nx_wire), 
        .b(ny_wire), 
        .cin(0), 
        .sum(add_xy_wire), 
        .cout(0)
    );

    mux16 f_mux (
        .a(add_xy_wire),
        .b(and_xy_wire),
        .sel(f),
        .out(out_wire)
    );

    mux16 no_mux (
        .a(out_wire),
        .b(~out_wire),
        .sel(no),
        .out(out)
    );

    assign zr = ~| out_wire;
    assign ng = out_wire[0];	
endmodule
