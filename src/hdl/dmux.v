`timescale 1ns / 1ps

module dmux(
    input in,
    input sel,
    output a,
    output b
    );

	assign a = (sel) ? in : 0;
	assign b = (sel) ? 0 : in;

endmodule

module dmux16(
    input [15:0] in,
    input sel,
    output [15:0] a,
    output [15:0] b
    );

	assign a = (sel) ? in : 0;
	assign b = (sel) ? 0 : in;

endmodule