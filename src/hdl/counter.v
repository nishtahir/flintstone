module counter16(
    input[15: 0] in,
    input clk, 
    input reset,
    input inc,
    input load,
    output[15:0] out
);
    reg [15:0] counter;
    
    always @(posedge clk, posedge reset) begin
        if(reset)
            counter <= 16'd0;
        else if (inc)
            counter <= counter + 16'd1;
        else if (load)
            counter <= in;
        else 
            counter <= counter;
    end 
    assign out = counter;
endmodule