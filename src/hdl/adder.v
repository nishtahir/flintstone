`timescale 1ns / 1ps

module half_adder (
    input a,
    input b,
    output sum,
    output carry
  );

  assign sum = a ^ b;  // xor
  assign carry = a & b;

endmodule

module full_adder (
        input a, 
        input b, 
        input cin, 
        output sum,
        output cout
    );
 
    wire sum_wire;
    wire carry_wire_1;
    wire carry_wire_2;

    assign cout = carry_wire_1 | carry_wire_2;

    half_adder half_adder_1 (
        .a(a),
        .b(b),
        .sum(sum_wire),
        .carry(carry_wire_1)
    );      

    half_adder half_adder_2 (
        .a(sum_wire),
        .b(cin),
        .sum(cout),
        .carry(carry_wire_2)
    );               
endmodule

module ripple_carry_adder16 (
        input [15:0] a, 
        input [15:0] b, 
        input cin, 
        output [15:0] sum, 
        output cout
    );
    
    wire[16:0] carry_wires;
    assign carry_wires[0] = cin;
    assign cout = carry_wires[16];
    
    parameter NUM_OF_A_MODULES = 16; // should be overriden from higher hierarchy
    genvar i;
    for (i = 0 ; i < NUM_OF_A_MODULES;  i= i + 1) begin
        full_adder fa(
            .a(a[i]), 
            .b(b[i]),
            .cin(carry_wires[i]), 
            .sum(sum[i]),
            .cout(carry_wires[i + 1])
        );
    end
    
endmodule
