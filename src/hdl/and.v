`timescale 1ns / 1ps

module and3(
    input a,
    input b,
    input c,
    output out
    );

    assign out = a & b & c;
endmodule

module and16(
    input [15:0] a,
    input [15:0] b,
    output [15:0] out
    );

	assign out = a & b;
endmodule
